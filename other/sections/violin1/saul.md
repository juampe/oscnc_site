+++
date        = "2013-06-21T11:27:27-04:00"
title       = "2nd Concertmaster"
description = "Saul Suarez Lobo"
tags        = [ "violin1", "concertmaster", "" ]
topics      = [ "Strings", "violin I" ]
slug        = "Saul"
project_url = "http://oscnc.org/violin1"
+++
# Saul Suarez Lobo 

Quick and easy performance analyzer library for [Go](http://golang.org/).

## Overview

Profesor de Violin y Viola en Oviedo

## Implementing Nitro

Using Nitro is simple. First, use `go get` to install the latest version
of the library.

	    $ go get github.com/spf13/nitro

Next, include nitro in your application.
