+++
date        = "2013-06-21T11:27:27-04:00"
title       = "1st Violins"
description = "1st violins of the OSCNC"
tags        = [ "violin1", "section", "" ]
topics      = [ "Strings", "violin I" ]
slug        = "Violin I"
project_url = "http://oscnc.org/violin1"
+++
# First violins of the OSCNC

## The list:

* [Vanessa Alfaro](/sections/violin1/vanessa.html).
* [Saul Suarez Lobo](/sections/violin1/saul.html).
* [Ignacio Prats](/sections/violin1/nacho.html).

## Implementing Nitro

Using Nitro is simple. First, use `go get` to install the latest version
of the library.

	    $ go get github.com/spf13/nitro

Next, include nitro in your application.
