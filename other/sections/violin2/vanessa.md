+++
date        = "2013-06-21T11:27:27-04:00"
title       = "Concertmaster"
description = "Vanessa Alfaro"
tags        = [ "violin1", "concertmaster", "" ]
topics      = [ "Strings", "violin I" ]
slug        = "Vanessa"
project_url = "http://oscnc.org/violin1"
+++
# Vanessa Alfaro 

Quick and easy performance analyzer library for [Go](http://golang.org/).

## Overview

Violin solista de la orquesta Municipal de Valencia
## Implementing Nitro

Using Nitro is simple. First, use `go get` to install the latest version
of the library.

	    $ go get github.com/spf13/nitro

Next, include nitro in your application.
