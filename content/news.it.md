+++
title = "Notizie"
weight = 40
draft = true
menuname = "News"
+++

{{< figure class="image main" src="/images/Chicago_Symphony_Hall_DSC7985_DARK-01.jpg" alt="Concerts Schedule" style="height: 200px;">}}

<h3 class="major">UPCOMING EVENTS</h3>

<div class="box">

## Streaming

### EL MESÍAS

**Venerdì 19 luglio 2024**, alle 18.40, Rai5 (canale televisivo di musica e arte) trasmetterà **EL MESÍAS di Kiko Argüello**, presentato in anteprima assoluta al Teatro Verdi di Trieste il 19 novembre 2023.
 
Per vedere il concerto su Rai5:

* dall'ITALIA (DVB): per guardare il concerto su Rai5, il canale televisivo digitale terrestre è il canale 23.
* da Internet: per vedere il concerto in streaming video, accedere alla piattaforma RaiPlay collegandosi da computer, smartphone, tablet o Smart TV a [www.raiplay.it](https://www.raiplay.it) e selezionando il canale Rai5.

Dopo la messa in onda del 19 luglio, THE MESSIAH sarà disponibile per 7 giorni attraverso la funzione Guida TV/Replay. 

Può essere rivisto su RaiPlay digitando EL MESÍAS nella sezione RICERCA. 

Il programma sarà disponibile su RaiPlay **fino al 31/12/2024**.

</div>

{{< socialLinks >}}
