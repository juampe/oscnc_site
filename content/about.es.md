+++
title = "El Compositor: Kiko Arguello"
weight = 10
draft = false
menuname = "Compositor"
+++

{{< figure class="image main" src="/images/KikoArguello_disco.jpg" alt="Kiko Arguello" style="height: 200px;">}}

<div style="text-align: justify">

**Francisco Gómez de Argüello Wirtz (Kiko Arguello)**, nació en León (España) el 9 de enero de 1939. Estudió Bellas Artes en la Academia de San Fernando de Madrid recibiendo el título de Profesor de Pintura y Diseño. En 1959 recibió el Premio Nacional Extraordinario de Pintura.

Después de una profunda crisis existencial, se produjo una seria conversión dentro de él que le llevó a dedicar su vida a Jesucristo y a la Iglesia. En 1960, con el escultor Coomontes y el artista de vidrio Muñoz de Pablos, fundó un grupo de investigación y desarrollo de arte sacro llamado "Gremio 62". Con este grupo hizo exposiciones en Madrid (Biblioteca Nacional) y, siendo nombrado por el Departamento de Asuntos Culturales, representa a España en la Exposición Universal de Arte Sacro en Royan (Francia) en 1960. En estas mismas fechas Kiko exhibió algunos de sus trabajos en Rolanda (Galena "Nouvelles images").

Convencido de que Cristo estaba presente en el sufrimiento de los últimos de la tierra, en 1964 fue a vivir entre los más pobres, trasladándose a una chabola hecha de madera en la zona de Palomeras Altas, en los suburbios de Madrid. Más tarde, Kiko se encontró alli con **Carmen Hernández**, e impulsados ​​por el ambiente de los pobres, se vieron empujados a encontrar una forma de predicación, una síntesis kerygmática-catequética que dió origen a la formación de una pequeña comunidad cristiana.

La primera comunidad nació así entre los pobres. Esta comunidad, en la que era visible el amor de Cristo crucificado, se convirtió en una "semilla" que, gracias al entonces Arzobispo de Madrid, Mons. Casimiro Morcillo, fue plantada en las parroquias de Madrid y más tarde en Roma y otras naciones. En contacto con las parroquias de los diferentes ambientes culturales, se ha ido esbozando poco a poco una forma de iniciación cristiana de adultos que descubre y recupera la riqueza del bautismo. **Kiko Argüello, Carmen Hernández y el sacerdote italiano, Padre Mario Pezzi** son hoy los responsables a nivel internacional del Camino Neocatecumenal, presente en 101 naciones de los 5 continentes. En numerosas ocasiones desde 1999, Kiko Argüello ha sido invitado por Juan Pablo II y por Benedicto XVI a participar en los Sínodos de los Obispos para la Iglesia Católica como auditor. En 2009 recibió el título de **Doctor Honoris Causa por el Instituto Pontífice Juan Pablo II Para Estudios sobre Matrimonio y Familia**, institución que tiene su sede en la Pontificia Universidad Lateranense de Roma. En 2011 fue nombrado **consultor del Pontificio Consejo para la Nueva Evangelización**, fundado por Benedicto XVI en 2010 para llevar a cabo la evangelización de países con ancestrales raíces cristianas, y que son las más afectadas por el fenómeno de la secularización.

La renovación pastoral que poco a poco se ha hecho en varias parroquias a través del Camino Neocatecumenal ha inspirado otros carismas, que se han concretado como instrumentos al servicio de la nueva evangelización, como son los Catequistas Itinerantes, los Seminarios Misioneros "Redemptoris Mater", las Familias En Misión, las "Missio ad Gentes" y las Comunidades en Misión, que han convertido al Camino Neocatecumenal en -podriamos decir- una vanguardia eclesial para el anuncio del Evangelio. De la misma manera se han desarrollado canales de renovación estética (música, pintura, arquitectura, vidrio-arte-obra, signos y ornamentos litúrgicos) capaces de expresar y hacer revivir los contenidos de la fe cristiana en el hombre de hoy.

De esta manera apareció el cancionero **"Resucitó"**, con alrededor de 300 cantos para su utilización en la liturgia que hoy se cantan en todas las lenguas de la Iglesia del mundo, algunos de las cuales se incluyeron en los grabaciones realizadas en los años 70 y otras fueron grabadas recientemente en ediciones como "María, Piccola María" (Italia, 1992) o "María, Paloma Incorrupta" (España, 2010).

</div>
A continuacion se enumeran algunas de sus obras de arte y arquitectura:

## Trabajo Artistico
### España
#### Madrid
* Abside de la Catedral de Madrid (La Almudena) con siete grandes frescos sobre los misterios de la vidad de Jesucristo y un gran Pantocrator central
* “La Ultima Cena” y “La Dormicion de la virgen”, Parroquia de Nuestra Señora del Transito
* “La Transfiguracion”, Parroquia de San Jose San Jose
* “Pentecostes”, Parroquia de La Paloma
* “Gran Corona Misterica”, Parroquia de Santa Catalina Labouré

#### Zamora
* “Nacimiento de Jesus, Bautismo y Resurreccion”, Parroquia de San Frontis

#### Murcia
* “Gran Retablo con los Misterios de la vida de Cristo”, Parroquia de San Pedro del Pinatar

### Italia

* Rome, Mural, Crypt in the Parish of the Canadian Martyrs: the “Holy Trinity” and the “Ascension of the Lord”, Parish of Sta. Frances Cabrini
* The “Apparition of Christ Risen to St. Thomas, Parish of St. Louis of Gonzague
* Porto S. Giorgio: “Mysteries of the Life of Christ”
* Florence: “Mysteric Crown”, Parish of St. Bartolo in Tutto
* Piacenza: “Mysteries of the Life of Christ”

### Israel

* Domus Galiaeae, Korazim, “The Final Judgement”

### China

* Shangai, Altarpiece of the Church of St. Francis Xavier

## Trabajos en arquitectura

* Israel: on top of the Mount of the Beatitudes, Domus Galilaeae
* Italy: Porto S.Giorgio, “International Center for the New Evangelization”
* Spain:
 * Madrid, Iglesia y Catecumenium de la Parroauia de Santa Catalina Labouré
 * Centro Neocatecumenal  “La Pizarra”, San Lorenzo del Escorial, Madrid
* Finland: Oulu, Parish Church of St. Ansgar
