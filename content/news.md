+++
title = "News"
weight = 40
draft = true
menuname = "News"
+++

{{< figure class="image main" src="/images/Chicago_Symphony_Hall_DSC7985_DARK-01.jpg" alt="Concerts Schedule" style="height: 200px;">}}

<h3 class="major">UPCOMING EVENTS</h3>

<div class="box">

## STREAMING

### EL MESÍAS
On **Friday, July 19, 2024**, at 6:40 p.m., Rai5 (the music and art television channel) will broadcast Kiko Argüello's **EL MESÍAS by Kiko Argüello**, premiered at the Teatro Verdi in Trieste on November 19, 2023.
 
To watch the concert on Rai5:

- from ITALY (DVB): to watch the concert on Rai5, the digital terrestrial television channel is channel 23.
- from the Internet: to watch the concert in streaming video, access the RaiPlay platform by connecting from your computer, smartphone, tablet or Smart TV to [www.raiplay.it](https://www.raiplay.it) and selecting the Rai5 channel.

After the broadcast on July 19, EL MESÍAS will be available for 7 days through the TV Guide/Replay function. 

It can be replayed on RaiPlay by typing EL MESÍAS in the SEARCH section. 

The program will be available on RaiPlay until **12/31/2024**.

</div>

You can get more up to date news in our Social Media links:

#### Social Media of the OSCNC

{{< socialLinks >}}
