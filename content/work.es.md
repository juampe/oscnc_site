+++
title = "La Sinfonia"
weight = 20
draft = false
menuname = "Sinfonia"
+++
{{< figure class="image main" src="/images/mariasottolacroce558x200-2.jpg" alt="Maria" style="height: 200px;">}}

## EL SUFRIMIENTO DE LOS INOCENTES

<div style="text-align: justify">

<p> Queridos hermanos: ¿Cómo pretender componer música? ¿Será una presunción mía? ¿O mi vanidad? Sea como fuere: "No dejes nunca de hacer el bien por miedo a la vanidad, porque eso viene del demonio', me dijo un vez un sacerdote anciano. "Hacer el bien... ¿Es un bien intentar componer música? ¿Por qué me excuso? Es verdad, me estoy excusando, perdonadme. Os presento una pequeña composición musical, que querría que fuese celebrativa, catequética también, diría, sobre el sufrimiento de los inocentes, sobre el sufrimiento de la Virgen. ¿Quizá la música logra decirnos algo más profundo sobre un tema tan importante...?


<p> El sufrimiento de los inocentes... Dijo el filósofo Sartre: "Ay de aquel a quien el dedo de Dios aplaste contra la pared"; y Nietzsche: "Si Dios existe y no ayuda a los que sufren, es un monstruo, y, si no puede ayudarles, no es Dios, no existe".

<p> Ser estrellados contra la pared. Hombres tirados en la calle, muertos de frío. Niños abandonados y recogidos en orfelinatos de horror, donde son violentados y abusados. Aquella mujer que conocí en aquel barrio, con Parkinson, abandonada por su marido, a quien su hijo enfermo mental golpeaba con un bastón, y que pedía limosna. Me quedé sobrecogido ante Jesús muerto en la Cruz, presente allí, en ella y en tantos otros y otros... Qué misterio el sufrimiento de tantos inocentes que cargan con el pecado de otros, incesto, una violencia inaudita, aquella fila de mujeres y niños desnudos hacia la cámara de gas, y aquel dolor profundo de uno de los guardianes que dentro de su corazón sentía una voz: "entra en la fila y ve con ellos a la muerte"; y no sabía de dónde le venía... Dicen que después del horror de Auschwitz ya no se puede creer en Dios. iNo! iNo es verdad! Dios se ha hecho hombre para cargar Él con el sufrimiento de todos los inocentes. Él es el inocente total, el cordero llevado al matadero sin abrir la boca, el que carga con los pecados de todos.

<p> En esta pequeña obra, se presenta a la Virgen María sometida al escándalo del sufrimiento de los inocentes en su carne, en la carne de su Hijo. "¡Oh, qué dolor!', canta una voz mientras una espada atraviesa su alma.

<p> Querríamos celebrar juntos, con estos trazos musicales, cuánto sostuvo un ángel a la Virgen, como a Jesús en el Huerto de los Olivos, cuando otro ángel le ayudó a beber el cáliz preparado para los pecadores. ¡Querríamos contemplar y sostener a la Virgen que acepta la espada que, según el profeta Ezequiel, Dios ha preparado por los pecados de su pueblo, y que ahora atraviesa el alma de esta pobre mujer!

<p> ¡María, María! iMadre de Dios! Santa Theotokos. iÁnimo! Tú eres la Madre del Dios que se hace pecado por nosotros y se ofrece por la salvación de todos. Madre de Dios y Madre nuestra. Cantemos todos.

</div>

## Algunos comentarios descriptivos de los movimientos de la Sinfonía

<div style="text-align: justify">

<p>La sinfonía consta de cinco movimientos. 

<p>El primer movimiento se llama “**Getsemaní**”, que era el nombre del huerto donde Nuestro Señor Jesucristo fue a rezar y a tener un terrible combate, porque allí se debía preparar para dar su vida por todos los hombres de la tierra. Allí, lleno de angustia y miedo, le dice a su Padre: "Abba! Si no es posible que pase este cáliz sin que yo lo beba, entonces que se haga tu voluntad." Tanto era su sufrimiento que Dios le envía un ángel para sostenerlo. Por eso, a mitad de este primer movimiento, las violinistas se ponen en pie significando la llegada de este ángel. Más tarde, se oye una cadencia de tambor, simbolizando a los soldados con Judas, el traidor, que se acercan para prenderlo, torturarlo y crucificarlo. Y el movimiento termina con el coro, cantando "¡Abba! ¡Padre!", que significa que Cristo acepta ser torturado y crucificado por todos los hombres.

<p>El segundo movimiento se llama “**Lamento**”. Os invito a contemplar a la Virgen, debajo de la cruz de su Hijo, sintiendo un dolor enorme. Se escuchará un arpa, simbolizando las lágrimas de la Virgen María.

<p>El tercer movimiento se llama “**Perdónales**”. Os invito a contemplar a Cristo crucificado, sufriente, que grita al Padre: “Perdónales!”, pidiendo perdón por todos los hombres. ¡Por eso un tenor cantara “Perdónales!” hacia el final del movimiento.

<p>El cuarto movimiento se llama “**La Espada**”. Esta “espada” es importante, ya que el profeta Ezequiel ve que su pueblo que ha cometido muchos pecados, de incesto, violencia, usura, asesinato, y Dios dice al profeta: “Profetiza una espada, que caerá sobre ellos”. Esta espada no es un castigo de Dios, es el producto de su maldad, porque Dios les había dicho que no se aliaran con Egipto y no han obedecido. No han querido obedecer a Dios y por eso el rey Nabucodonosor, irritado contra este pueblo, ha puesto cerco a Jerusalén, ha destruido el Templo, y por medio de esta “espada” la sangre corre ahora por toda la ciudad. Esta profecía es completamente cierta para nosotros, hoy. No han pasado 70 años desde dos guerras mundiales que han provocado 60 millones de muertos, con 6 millones de judíos muertos en campos de concentración, dos bombas atómicas sobre Japón: Hiroshima y Nagasaki… Entonces, esta “espada” presente en nuestra sociedad por los pecados de los hombres, ha querido ser aceptada por la Virgen María para salvarnos, y por eso el Evangelio dice que “una espada atravesará el alma de la Virgen”, cuando está debajo de la cruz. Otro aspecto de la “espada” son los desastres naturales, los terremotos, el “tsunami”, la central nuclear de Fukusima. ¿Por qué, esto? ¿Es un castigo de Dios por nuestros pecados? Le preguntan a Jesucristo, cuando un terremoto hizo derrumbarse una torre, y todos murieron: “¿Por qué esto? ¿Es porque han sido unos pecadores, unos malvados, y Dios les castiga?” Y Cristo dice: “No. No es por sus pecados. ¿Os creéis que sois mejores que esos hombres? No, os lo digo.” Esta es una palabra que llama a todos los hombres a conversión, a volverse a Dios. Con estos hechos que han sucedido en Japón, Dios está hablando a toda la humanidad. Digo que debemos todos repensar nuestra vida, toda fundamentada en el placer, el dinero y el orgullo. Es una palabra de ayuda para nosotros, para descubrir que nuestra vida es precaria, que podemos morir en cualquier momento.

<p>Por último, el quinto movimiento se llama “**Resurrexit**”, porque a Cristo, muerto por todos los hombres, Dios lo ha resucitado de la muerte, y nosotros estamos anunciando a todo el mundo la resurrección, la victoria sobre la muerte. Los cristianos no morimos, y también para vosotros es la esperanza de la resurrección y la vida eterna, la felicidad eterna en el Cielo. 

<p>¡Arigato!

</div>
***Kiko Arguello. Koriyama (Japon), 6 de mayo de 2016)***
