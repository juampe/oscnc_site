+++
title = "Noticias"
weight = 40
draft = true
menuname = "News"
+++

{{< figure class="image main" src="/images/Chicago_Symphony_Hall_DSC7985_DARK-01.jpg" alt="Concerts Schedule" style="height: 200px;">}}

<h3 class="major">UPCOMING EVENTS</h3>

<div class="box">

## Streaming

### EL MESÍAS
El **viernes 19 de julio de 2024**, a las 18.40 horas, Rai5 (el canal de televisión de música y arte) emitirá **EL MESÍAS de Kiko Argüello**, estrenado en el Teatro Verdi de Trieste el 19 de noviembre de 2023.
 
Para ver el concierto en Rai5:

- desde ITALIA (DVB): para ver el concierto en Rai5, el canal de televisión digital terrestre es el canal 23.
- por Internet: para ver el concierto en vídeo streaming, accede a la plataforma RaiPlay conectándote desde tu ordenador, smartphone, tablet o Smart TV a [www.raiplay.it](https://www.raiplay.it) y seleccionando el canal Rai5.

Tras la emisión el 19 de julio, EL MESÍAS podrá visionarse durante 7 días a través de la funcion TV Guía/Replay. 

Se puede volver a ver en RaiPlay tecleando EL MESÍAS en la seccion BUSCAR. 

El programa estará disponible en RaiPlay hasta el **31/12/2024**.

</div>

{{< socialLinks >}}
