+++
title = "The Symphony"
weight = 20
draft = false
menuname = "Sinfonia"
+++
{{< figure class="image main" src="/images/mariasottolacroce558x200-2.jpg" alt="Maria" style="height: 200px;">}}

## THE SUFFERING OF THE INNOCENTS

<div style="text-align: justify">

<p> Dear Brothers and Sisters: How could I presume to compose music? Is it because of my pride? Or my vanity? Be that as it may: "Never avoid doing good for the fear of vanity, for this comes from the devil" an elderly priest once told me. "To do good ... " Is it good to compose music? 1 present to you a small musical composition, which I would like to be a homage to the suffering of the innocents. Can music, perhaps, say something more profound on such a significant theme?

<p> The suffering of the innocents ... The philosopher Sartre said: "Woe to the man whom the finger of God crushes against the wall," and Nietzsche: "If God exists and does not help those who suffer, he is a monster, and if he cannot help them, he is not God, he does not exist."

<p> To be crushed against the wall. Men lying on the street, dying of cold. Children abandoned and housed in horrific orphanages, where they suffer violence and are abused. That woman, whom I met in that neighborhood, suffering from Parkinson's disease, abandoned by her husband, whose mentally-ill son beat her with a stick, and was begging for alms. I was overwhelmed in front of Jesus dead on the Cross, present there, in that woman and so many others ... What a mystery the suffering of so many innocents who bear the sins of others, incest, a violence unheard of, the line of naked women and children going towards the gas chamber, and the deep pain of one of the guards who, from within his heart, heard a voice: "Get in line with them and go with them to die; "and he did not know where this voice came from ... It is said that after the Horror of Auschwitz, it is no longer possible to believe in God. No! It is not true!

<p> In this small work, the Virgin Mary presents herself submitted to the scandal of the suffering of the innocents in her own flesh, in the flesh of her son. "Oh, Such Pain!" sings a voice while a sword pierces her soul.

<p> We would like to contemplate and sustain the Virgin who accepts the sword which, according to the prophet Ezekiel, God had prepared for the sins of his people, which now pierces the soul of this poor woman!

<p> Maria, we shall sing with you "Shema Israel" together with all the Hebrew mothers, whom as you did, have seen their own children die. Let all of us sing.

</div>

## Alcuni commenti di Kiko per descrivere la Sinfonia

<div style="text-align: justify">

<p>La sinfonia è composta da 5 movimenti.

<p>Il primo movimento si chiama “**Getsemani**”, il nome del giardino dove nostro Signore Gesù Cristo andò a pregare per sostenere un terribile combattimento perché si doveva preparare per dare la sua vita per tutti gli uomini della terra. Lì, pieno di angoscia e paura, dice a suo Padre: “Abbà! Padre, se non è possibile che passi da me questo calice senza che io lo beva, allora si faccia la tua volontà”. La sua sofferenza era così terribile che Dio gli manda un angelo per sostenerlo. Ed ecco perché, nella prima metà di questo movimento, per far capire che Dio manda l’angelo, le violiniste si alzano e suonano in piedi. Segue un rullo di tamburo militare cadenzato che indica che i soldati accompagnati da Giuda, il traditore, stanno arrivando per catturare Gesù, torturarlo e crocifiggerlo. E il movimento si conclude con il coro che canta “Abbà! Padre!”, che vuol dire che Cristo accetta di essere torturato e crocifisso per tutta l’umanità.

<p>Il secondo movimento si chiama “**Lamento**”. Vi invito a contemplare la Vergine sotto la croce di suo Figlio che sta soffrendo profondamente. Sentirete la voce dell’arpa, i suoi accordi rappresentano le lacrime della Vergine Maria.

<p>Il terzo movimento si chiama “**Perdonales**” (Perdonali ). Vi invito a contemplare un crocifisso, Gesù Cristo sofferente che grida a suo Padre: “Perdonali!”, e chiede il perdono per tutti gli uomini. Per questo, nel finale del movimento, un tenore canterà “Perdonales”.

<p>Il quarto movimento è “**La Espada**” (La Spada). Questa “spada” è importante, poiché il profeta Ezechiele ha visto che il suo popolo ha commesso molti peccati, incesti, violenze, usura, omicidi e Dio dice al profeta: “Profetizza una spada che cadrà su di loro”. Questa spada non è un castigo di Dio, è il prodotto della loro malvagità. Dio aveva detto loro di non allearsi con l’Egitto ma essi non hanno voluto obbedirgli e per questo, il re Nabucodonosor, irritato con il popolo, circonda Gerusalemme, distrugge il Tempio e attraverso questa “spada” il sangue scorre per tutta la città. Questa è una profezia assolutamente vera per noi oggi. Sono passati quasi 70 anni da quando due guerre mondiali hanno causato sessanta milioni di morti, sei milioni di Ebrei sterminati nei campi di concentramento e due bombe atomiche sganciate sul Giappone, a Hiroshima e Nagasaki… Dunque, questa spada, presente nella nostra società a causa dei peccati degli uomini, è stata accettata dalla Vergine Maria per salvarci. Per questa ragione il Vangelo dice che ”una spada trafigge l’anima della Vergine Maria” quando sta sotto la croce. 
Un altro aspetto di questa “spada” sono i disastri naturali, i terremoti, gli “Tsunami”, l’incidente nucleare di Fukushima. Perché questo? È una punizione di Dio per i nostri peccati? Lo chiedono a Gesù dopo che un terremoto provocò il crollo di una torre e tutti morirono. Gli domandano: “Perché è accaduto questo?. Perché erano peccatori, malvagi, e Dio li ha puniti?”. E Gesù risponde: “No, non è a causa dei loro peccati. Voi pensate di essere migliori di quegli uomini? No vi dico!”. 
 Questa è una parola che chiama tutti  gli uomini a conversione, a ritornare a Dio. Con questi fatti che sono accaduti in Giappone, Dio sta parlando a tutta l’umanità. Io vi dico che tutti voi dovreste ripensare alla vostra vita tutta basata sul piacere, sul denaro e sull’orgoglio. Questa è una parola che ci viene in aiuto per farci scoprire che la nostra vita è precaria e che noi possiamo morire in qualsiasi momento.

<p>E l’ultimo movimento, il quinto, si chiama “**Resurrexit**”, perché Cristo, che è morto per tutti gli uomini, Dio lo ha risuscitato e noi stiamo annunciando al mondo intero la resurrezione, la vittoria sopra la morte. I Cristiani non muoiono e anche per voi esiste la speranza della resurrezione e della vita eterna, della felicità eterna nel Paradiso”.

<p>Arigatò!

</div>
***Kiko Arguello. Koriyama , 6 maggio 2016***
