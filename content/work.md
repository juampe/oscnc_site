+++
title = "The Symphony"
weight = 20
draft = false
menuname = "Symphony"
+++
{{< figure class="image main" src="/images/mariasottolacroce558x200-2.jpg" alt="Maria" style="height: 200px;">}}

## THE SUFFERING OF THE INNOCENTS

<div style="text-align: justify"> 

Dear Brothers and Sisters: How could I presume to compose music? Is it because of my pride? Or my vanity? Be that as it may: "Never avoid doing good for the fear of vanity, for this comes from the devil" an elderly priest once told me. "To do good ... " Is it good to compose music? 1 present to you a small musical composition, which I would like to be a homage to the suffering of the innocents. Can music, perhaps, say something more profound on such a significant theme?

<p> The suffering of the innocents ... The philosopher Sartre said: "Woe to the man whom the finger of God crushes against the wall," and Nietzsche: "If God exists and does not help those who suffer, he is a monster, and if he cannot help them, he is not God, he does not exist."

<p> To be crushed against the wall. Men lying on the street, dying of cold. Children abandoned and housed in horrific orphanages, where they suffer violence and are abused. That woman, whom I met in that neighborhood, suffering from Parkinson's disease, abandoned by her husband, whose mentally-ill son beat her with a stick, and was begging for alms. I was overwhelmed in front of Jesus dead on the Cross, present there, in that woman and so many others ... What a mystery the suffering of so many innocents who bear the sins of others, incest, a violence unheard of, the line of naked women and children going towards the gas chamber, and the deep pain of one of the guards who, from within his heart, heard a voice: "Get in line with them and go with them to die; "and he did not know where this voice came from ... It is said that after the Horror of Auschwitz, it is no longer possible to believe in God. No! It is not true!

<p> In this small work, the Virgin Mary presents herself submitted to the scandal of the suffering of the innocents in her own flesh, in the flesh of her son. "Oh, Such Pain!" sings a voice while a sword pierces her soul.

<p> We would like to contemplate and sustain the Virgin who accepts the sword which, according to the prophet Ezekiel, God had prepared for the sins of his people, which now pierces the soul of this poor woman!

<p> Maria, we shall sing with you "Shema Israel" together with all the Hebrew mothers, whom as you did, have seen their own children die. Let all of us sing.

</div>

## Some descriptive comments of the Symphony's Movements

<div style="text-align: justify">
<p>  The symphony consists of five movements.

<p>The first movement was called “**Getsemaní**” (Gethsemane), which was the name of the garden where Our Lord Jesus Christ went to pray and he fought a terrible battle, because he had to prepare to give his life for all the men over the earth. There, full of fear, he says to his Father: "Abba! If it is not possible for this chalice to pass without drinking it, then may your will be done." Hiis suffering was so terrible that God sent him an angel to support him. That is why, in the middle of this first movement, the female violinists stand up and play, symbolizing the arrival of the angel. Later, a snare drum cadenza indicate the soldiers with Judas the traitor are coming, to seize him, torture him and crucify him. And the movement ends with the choir singing "Abba! Father! ", Which means that Christ accepts to be tortured and crucified by all the mankind.

<p>The second movement is called “**Lamento**” (Lament). I invite you to contemplate the Virgin, under the cross of her Son, feeling a deep pain. You will hear a harp, symbolizing the tears of the Virgin Mary.

<p>The third movement is called “**Perdónales**” (Forgive them). I invite you to contemplate a crucified, suffering Jesuschrist, who cries out to the Father: "Forgive them!", asking for forgiveness for all men. For this reason a tenor will sing "Perdónales!" towards the end of the movement.

<p>The fourth movement is called “**La Espada**” (The Sword). This "sword" is important, since the prophet Ezekiel sees that his people have committed many sins, from incest, violence, usury, murder, and God says to the prophet: "Prophesy a sword that will fall on them." This sword is not a punishment of God, it is the product of their wickedness, because God had told them not to ally with Egypt, and they have not obeyed. They did not want to obey God, and that is why King Nebuchadnezzar, irritated against this people, has surrounded Jerusalem, destroyed the Temple, and through this "sword" the blood now flows throughout the city. This prophecy is completely true for us today. Nearly 70 years have passed since two world wars that have caused 60 million deaths, with 6 million Jews killed in concentration camps, two atomic bombs dropped on Japan: Hiroshima and Nagasaki ... Then this "sword", present in our society by the sins of men, has been accepted by the Virgin Mary to save us, and for this reason the Gospel says that "a sword pierced the soul of the Virgin", when she is under the cross. Another aspect of this "sword" are natural disasters, earthquakes, the tsunami, the Fukusima nuclear power plant. Why this? Is it a punishment from God because of our sins? They ask Jesus Christ, when an earthquake caused a tower to collapse, and they all died: "Why this? Is it because they have been sinners, some wicked, and God punishes them?" And Christ says: "No. It is not because of their sins. Do you think you are better than these men? No, I tell you." This is a word that calls all men to conversion, to turn to God. With these facts that have happened in Japan, God is speaking to all mankind. I say we should all rethink our life, all based on pleasure, money and pride. It is a word of help for us, to discover that our life is precarious, that we can die at any moment.

<p>Finally, the fifth movement is called “**Resurrexit**”, because Christ, who died for all men, God has raised him from the death, and we are announcing to the whole world the resurrection, the victory over death. Christians do not die, and also for you it is the hope of resurrection and eternal life, eternal happiness in Heaven.

<p>Arigato!

</div>
***Kiko Arguello. Koriyama (Japan), May 6th 2016***
