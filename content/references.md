+++
date = "2017-05-30T20:31:59+02:00"
title = "References"
weight = 20
draft = false
menuname = "References"
+++

<h2>Press Releases</h2>
<ul>
	<li><a href="http://ilpiccolo.gelocal.it/trieste/cronaca/2017/06/25/news/trieste-in-piazza-unita-l-omaggio-in-musica-alla-shoah-1.15536683">Il Piccolo di Trieste, June 25gth, 2017</a></li>
	<li><a href="http://www.triesteprima.it/cronaca/piazza-unita-blindata-per-la-sofferenza-degli-innocenti-8500-spettatori-da-tutta-italia-e-paesi-confinanti.html">Trieste Prima, June 25th, 2017</a></li>
	<li><a href="http://www.triesteprima.it/eventi/sette-anni-dopo-concerto-dei-presidenti-trieste-torna-a-unire-con-la-sofferenza-degli-innocenti.html">Trieste Prima, June 1st, 2017</a></li>
 	<li><a href="http://www.larazon.es/religion/el-vaticano-acogio-la-sinfonia-el-sufrimiento-de-los-inocentes-de-kiko-arguello-BM13682726">La Razon, October 7th, 2016</a></li>
 	<li><a href="http://www.religionenlibertad.com/la-sinfonia-el-sufrimiento-de-los-inocentes-de-kiko-argello-este-29818.htm">Religion en Libertad, June 21st, 2013</a></li>
 	<li><a href="https://es.zenit.org/articles/gran-exito-de-la-sinfonia-de-kiko-arguello-el-sufrimiento-de-los-inocentes/">Zenit, May 14th,  2012</a></li>
 	<li><a href="http://www.religionconfidencial.com/tribunas/Sinfonia-sufrimiento-inocentes_0_1636036396.html">Religion Confidencial, June 11th,  2011</a></li>
 	<li><a href="http://jewishweek.timesofisrael.com/a-concert-of-reconciliation/">New York Jewish Week, May 1st, 2012</a></li>
 	<li><a href="https://www.cmc-terrasanta.com/en/video/archaeology-culture-and-other-religions-8/concert-in-bethlehem-the-suffering-of-the-innocents-874.html">CMC Terra Santa, December 28th, 2011</a></li>
 	<li><a href="https://patch.com/new-jersey/teaneck/concert-wednesday-in-honor-of-holocaust-victims">Teaneck Patch, May 8th, 2012</a></li>
 	<li><a href="http://sufferingoftheinnocents.com/englishsimphony.pdf">Jerusalem Post</a></li>
 	<li><a href="http://es.radiovaticana.va/news/2015/05/09/kiko_arg%C3%BCello_agradece_al_papa_su_mensaje_por_el_encuentro_/1142915">Radio Vaticana, May 9th, 2015</a></li>
</ul>
<h2>Coments about the Musical Work</h2>
<ul>
 	<li><a href="https://es.zenit.org/?p=29580">Prof. Ignacio Prats (CEU), Zenit, June 29th, 2013</a></li>
 	<li><a href="http://www.camineo.info/news/268/ARTICLE/18158/2011-12-12.html">Prof. Desiderio Parrilla, CAMINEO, December 12th, 2011</a></li>
 	<li><a href="http://www.frontpagemag.com/fpm/131027/suffering-innocents-jamie-glazov">Giuseppe Gennarini, Frontpage, May 3rd, 2012</a></li>
 	<li><a href="http://www.cardinalseansblog.org/2012/05/11/the-suffering-of-the-innocents/">Boston Cardinal Sean O´Malley blog, May 11th, 2012</a></li>
</ul>
<h2>Concert promotion and news</h2>
<ul>
 	<li><a href="http://www.suntory.com/culture-sports/suntoryhall/schedule/detail/20160507_M_3.html">Suntory Hall, Tokio, May 7th,  2016</a></li>
 	<li><a href="http://www.sufferingoftheinnocents.com/">Boston Symphony Hall, Lincoln Center of New York &amp; Chicago Orchestra Hall concerts</a></li>
 	<li><a href="https://www.yelp.com/events/new-york-the-suffering-of-the-innocents-lincoln-center-symphony-to-honor-victims-of-the-holocaust">New York concert</a> (yelp)</li>
</ul>
