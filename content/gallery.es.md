+++
date = "2017-05-30T20:48:45+02:00"
draft = false
title = "Galeria Multimedia"
weight = 10
menuname = "Galeria"
+++

## Fotografias 

* [Flickr Album of the OSCNC](https://www.flickr.com/photos/neocatechumenaleiter/albums/72157680654138850)
* [OSCNC Concert in Auschwitz-Birkenau, June 2013](https://www.flickr.com/photos/catholicism/albums/72157634289221685)  

## Videos

* Youtube:
  * [Madrid, Cat. La Almudena concert](https://www.youtube.com/watch?v=aUX-QFGkiDg)
  * [New York Avery Fisher Hall concert](https://www.youtube.com/watch?v=sAJ1JG7BmP8)
  * [Auschwitz-Birkenau concert](https://www.youtube.com/watch?v=THl2xiJ8Wv4)
  * [Budapest State Opera concert](https://www.youtube.com/watch?v=yxY6B0oaMF8)
  * [Paul VI Hall, Vatican City concert](https://www.youtube.com/watch?v=JBHS5jhvvpQ)
  * [Catedrale di Soria concerto](https://www.youtube.com/watch?v=PUjo9llPUL0&t=5s)
  * Berliner Philharmonie:
     * [Berliner Philharmonie, Sinfonia](https://www.youtube.com/watch?v=9FAfVz4ZUaI&t=647s)
     * [Berliner Philharmonie, Hijas de Jerusalen](https://www.youtube.com/watch?v=MO7jFmlvYA8&t=80s)

* Vimeo:
  * [Koriyama City Hall concert](https://vimeo.com/214821592/f7d0e2b76c)
  * [OSCNC rehearsals for Japan tour](https://vimeo.com/213407334/4a5b15aed5)
