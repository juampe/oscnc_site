+++
date = "2017-05-30T20:36:01+02:00"
title = "Referencias"
weight = 20
draft = false
menuname = "Referencias"
+++

<h2>Reseñas de prensa</h2>
<ul>
	<li><a href="http://ilpiccolo.gelocal.it/trieste/cronaca/2017/06/25/news/trieste-in-piazza-unita-l-omaggio-in-musica-alla-shoah-1.15536683">Il Piccolo di Trieste, June 25gth, 2017</a></li>
	<li><a href="http://www.triesteprima.it/cronaca/piazza-unita-blindata-per-la-sofferenza-degli-innocenti-8500-spettatori-da-tutta-italia-e-paesi-confinanti.html">Trieste Prima, June 25th, 2017</a></li>
	<li><a href="http://www.triesteprima.it/eventi/sette-anni-dopo-concerto-dei-presidenti-trieste-torna-a-unire-con-la-sofferenza-degli-innocenti.html">Trieste Prima, June 1st, 2017</a></li>
 	<li><a href="http://www.larazon.es/religion/el-vaticano-acogio-la-sinfonia-el-sufrimiento-de-los-inocentes-de-kiko-arguello-BM13682726">La Razon, 7 de octubre de 2016</a></li>
 	<li><a href="http://www.religionenlibertad.com/la-sinfonia-el-sufrimiento-de-los-inocentes-de-kiko-argello-este-29818.htm">Religion en Libertad, 21 de junio de 2013</a></li>
 	<li><a href="https://es.zenit.org/articles/gran-exito-de-la-sinfonia-de-kiko-arguello-el-sufrimiento-de-los-inocentes/">Zenit, 14 de mayo de 2012</a></li>
 	<li><a href="http://www.religionconfidencial.com/tribunas/Sinfonia-sufrimiento-inocentes_0_1636036396.html">Religion Confidencial, 20 de junio de 2011</a></li>
 	<li><a href="http://jewishweek.timesofisrael.com/a-concert-of-reconciliation/">New York Jewish Week, 1 de mayo de 2012</a></li>
 	<li><a href="https://www.cmc-terrasanta.com/en/video/archaeology-culture-and-other-religions-8/concert-in-bethlehem-the-suffering-of-the-innocents-874.html">CMC Terra Santa, 28 de diciembre de 2011</a></li>
 	<li><a href="https://patch.com/new-jersey/teaneck/concert-wednesday-in-honor-of-holocaust-victims">Teaneck Patch, 8 de mayo de 2012</a></li>
 	<li><a href="http://sufferingoftheinnocents.com/englishsimphony.pdf">Jerusalem Post</a></li>
 	<li><a href="http://es.radiovaticana.va/news/2015/05/09/kiko_arg%C3%BCello_agradece_al_papa_su_mensaje_por_el_encuentro_/1142915">Radio Vaticana, 9 de mayo 2015</a></li>
</ul>
<h2>Comentarios sobre la obra musical</h2>
<ul>
 	<li><a href="https://es.zenit.org/?p=29580">Ignacio Prats (CEU), Zenit, 29 de junio 2013</a></li>
 	<li><a href="http://www.camineo.info/news/268/ARTICLE/18158/2011-12-12.html">Desiderio Parrilla, CAMINEO 11 de Diciembre de 2012</a></li>
 	<li><a href="http://www.frontpagemag.com/fpm/131027/suffering-innocents-jamie-glazov">Giuseppe Gennarini, Frontpage, 3 de mayo de 2012</a></li>
 	<li><a href="http://www.cardinalseansblog.org/2012/05/11/the-suffering-of-the-innocents/">Blog del Cardenal Sean O´Malley (Boston), 11 de mayo de 2012</a></li>
</ul>
<h2>Notificaciones y promocion de conciertos</h2>
<ul>
 	<li><a href="http://www.suntory.com/culture-sports/suntoryhall/schedule/detail/20160507_M_3.html">Suntory Hall, Tokio, 7 de mayo de 2016</a></li>
 	<li><a href="http://www.sufferingoftheinnocents.com/">Boston Symphony Hall, Lincoln Center of New York &amp; Chicago Orchestra Hall concerts</a></li>
 	<li><a href="https://www.yelp.com/events/new-york-the-suffering-of-the-innocents-lincoln-center-symphony-to-honor-victims-of-the-holocaust">New York concert</a> (yelp)</li>
</ul>
