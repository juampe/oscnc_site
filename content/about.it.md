+++
title = "Il Compositore: Kiko Arguello"
weight = 10
draft = false
menuname = "Compositore"
+++
{{< figure class="image main" src="/images/KikoArguello_disco.jpg" alt="Kiko Arguello" style="height: 200px;">}}

<div style="text-align: justify">

**Francisco-José Gómez de Argüello Wirtz (Kiko Arguello)**, was born in Leon, Spain on January 9th, 1939. He studied Fine Arts in the Academy of San Fernando of Madrid receiving the title of Professor of Painting and Design. In 1959 he received the Extraordinary National Prize for Painting.

After a deep existential crisis, a serious conversion happened inside of him which brings him to dedicate his life to Jesus Christ and the Church. In 1960, with the sculptor Coomontes and the Glass Artist Muñoz de Pablos, he founded a group of research and development of Sacred Art named “Gremio 62”. With this group he made exhibits in Madrid (National Library), and represents Spain, being named by the Department of Cultural Affairs, in the Universal Exhibit of Sacred Art in Royan (France) in 1960. In these same dates Kiko exhibited some of his works in Rolanda (Galena “Nouvelles images”).

Convinced that Christ was present in the suffering of the Last Ones of the earth, in 1964 he went to live amongst the poorest, moving into a shanty made out of wood in Palomeras Altas, in the suburbs of Madrid. Later, Kiko met **Carmen Hernandez** and, driven by the environment of the poor, they saw themselves forced to find a form of preaching, a kerygmatic-catechetical synthesis, which gave birth to the formation of a small Christian community.

The first community was thus born amongst the poor. This community in which was made visible the love of Christ crucified, became a “seed” which, thanks to the then Archbishop of Madrid, Mons. Casimiro Morcillo, was planted in the parishes of Madrid and later on in Rome and other nations. In contact with the parishes of different cultural environments, little by little a way of adult Christian formation was outlined which discovers and recovers the richness of Baptism. Kiko Argüello, Carmen Hernandez and the Italian priest Rev. Mario Pezzi, are today the responsible at the international level for the Neocatechumenal Way, which is present in 101 nations in the 5 continents. Kiko Argüello has been invited in numerous occasions by John Paul II as well as Benedict XVI to participate to the Synod of Bishops for the Catholic Church as auditors since 1999. In 2009 he received the title of **Doctor Honoris Causa by the Pontfical Institute John Paul II for Studies on Marriage and Family** , an institution that has its center in the Pontifical Lateran University in Rome. In 2011 he was named consultor of the Pontifical Council for the New Evangelization, founded by Benedict XVI in 2010 to bring about the evangelization of countries with ancient Christian roots which are most a ected by the phenomenon of secularization.

The pastoral renewal which little by little has been brought on in various parishes through the Neocatechumenal Way, has inspired other charisms which have been concretized into instruments at the service of the new evangelization, such as Itinerant Catechists, Missionary Seminaries “Redemptoris Mater”, Families in Mission, Missio ad Gentes, Communities in Mission, which have converted the Neocatechumenal Way into – we can say – an ecclesial vanguard for the announcement of the gospel. In the same way channels of esthetic renewal have been developed (music, painting, architecture, glass-art-work, signs and liturgical ornaments) which are able to express and revive the contents of Christian faith in today’s man.

In this way the songbook “Resucitó” appeared, with around 300 songs for the liturgy that today are sung in all languages in the Church of the world, some of which were included in records already in the 1970’s and others were recently recorded such as “Maria, Piccola Maria” (Italy, 1992) or “Maria, Paloma Incorrupta” (Spain, 2010).

</div>
Here some of his art and architectural works are listed:

## Art Work
### Spain
#### Madrid
* Apse of the Cathedral of Madrid (La Almudena) with seven great frescos about the mysteries of the life of Christ and with a central “Pantocrator”
* “The Last Supper”, and “The Dormition of the Virgin”, Parish of Nuestra Señora del Transito;
* “The Transfiguration”, Parish of San Jose;
* “Pentecost”, Parish of La Paloma;
* “Great Mysteric Crown” Parish of St. Catalina Labouré;

#### Zamora
* “Birth of Jesus, Baptism and Resurrection””, Parish of San Frontis;

#### Murcia
* “Great Altarpiece with the mysteries of the life of Christ” Parish of St. Peter of Pinatar.

### Italy

* Rome, Mural, Crypt in the Parish of the Canadian Martyrs: the “Holy Trinity” and the “Ascension of the Lord”, Parish of Sta. Frances Cabrini;
* The “Apparition of Christ Risen to St. Thomas, Parish of St. Louis of Gonzague;
* Porto S. Giorgio: “Mysteries of the Life of Christ”;
* Florence: “Mysteric Crown”, Parish of St. Bartolo in Tutto;
* Piacenza: “Mysteries of the Life of Christ”

### Israel

* Domus Galiaeae, Korazim, “The Final Judgement”.

### China

* Shangai, Altarpiece of the Church of St. Francis Xavier.

## Architectural Works

* Israel: on top of the Mount of the Beatitudes, Domus Galilaeae.
* Italy: Porto S.Giorgio, “International Center for the New Evangelization”.
* Spain:
 * Madrid, Church and Catechumenium of the Parish of St. Catherine Labouré;
 * Neocatechumenal Center “La Pizarra”, Madrid.
* Finland: Oulu, Parish Church of St. Ansgar.
