+++
title = "The CNC Orchestra"
weight = 40
draft = false
menuname = "Orchestra"
+++

<div id="TOP">
<ul class="actions">
  <li><a href="#guitharp" class="button special">Guitar/Harp</a></li>
  <li><a href="#strings" class="button small">Strings</a></li>
  <li><a href="#woodwind" class="button special">Woodwind</a></li>
</ul>
<ul class="actions">
  <li><a href="#brass" class="button special">Brass</a></li>
  <li><a href="#percussion" class="button">Percussion</a></li>
</ul>
</div>

## Conductors

### Principal

<a href="http://tomashanus.com/">Tomáš Hanus</a>

### Assistant 

<a href="https://www.conspaganini.it/users/luciano-di-giandomenico">Luciano di Giandomenico</a>

## Strings

<div class="table-wrapper" id="strings">
  <table class="alt">
    <thead>
      <tr>
        <th>Violin I</th>
        <th>Violin II</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Vanessa Alfaro (concertmaster)</td>
        <td>Virginia Leon (leader)</td>
      </tr>
      <tr>
        <td>Saul Suarez Lobo (2nd concertmaster)</td>
        <td>Inmaculada Martin (principal)</td>
      </tr>
      <tr>
        <td>Saray Prados  Bravo</td>
        <td>Clara Perez Meroño (principal)</td>
      </tr>
      <tr>
        <td>Daniel Hernando Vicario</td>
        <td>Sara Vega Moreno  </td>
      </tr>
      <tr>
        <td>David Urdiales del Campo  </td>
        <td>Raquel Hernando Romero</td>
      </tr>
      <tr>
        <td>Sofia Carmona Hernando  </td>
        <td>Irene Gonzalez Fernandez</td>
      </tr>
      <tr>
        <td>Ignacio Prats Arolas </td>
        <td>Agatha Frysz</td>
      </tr>
      <tr>
        <td>Janez Bokavsek  </td>
        <td>Marien Rubio Aldehuela</td>
      </tr>
      <tr>
        <td>David Marzoli </td>
        <td>Mateo Tuñon Granda</td>
      </tr>
      <tr>
        <td>David Garcia-Amado </td>
        <td>Maria di Giandomenico</td>
      </tr>
      <tr>
        <td>Giulia Sciancalepore </td>
        <td>Paula Rivas Cheliz </td>
      </tr>
      <tr>
        <td>Zuzana Jurczak</td>
        <td>Peter Gajdoš </td>
      </tr>
      <tr>
        <td>Lorenzo Morelli</td>
        <td>Tomas Michael Hanus </td>
      </tr>
      <tr>
        <td>Ines Gomez Segui  </td>
        <td>Judita Gajdošová</td>
      </tr>  
      <tr>
        <td>Esteban Turiel Pascual  </td>
        <td>Nuno Chagas</td>
      </tr>  
      <tr>
        <td>Gregor Dietrich  </td>
        <td>Isabel Robles Gaitero</td>
      </tr>  
    </tbody>
  </table>

  <table class="alt">
    <thead>
      <tr>
        <th>Viola        </th>
        <th>Violoncelli/Violonchelos</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Gabriele Croci (principal) </td>
        <td>Maria Laura Zingarelli (principal)</td>
      </tr>
      <tr>
        <td>Luigi Moccia (principal)   </td>
        <td>Elias Leceta (principal)</td>
      </tr>
      <tr>
        <td>Jorge Gallardo Suarez </td>
        <td>Jesus Vazquez Lopez</td>
      </tr>
      <tr>
        <td>Maria Asensio Nogueira </td>
        <td>Miriam Rivera Salmeron </td>
      </tr>
      <tr>
        <td>Maurizio Fortuna </td>
        <td>Pedro Hernandez Garriga</td>
      </tr>
      <tr>
        <td>Renato Notaro </td>
        <td>Miguel Gonzalez Fernandez</td>
      </tr>
      <tr>
        <td>Giovanni Petrella </td>
        <td>Andrea Vega Moreno</td>
      </tr>
      <tr>
        <td>Davide Vinciguerra</td>
        <td>Ana Maria Morales Tent </td>
      </tr>
      <tr>
        <td>Juan Urdiales del Campo</td>
        <td>Maria Suarez Lopez</td>
      </tr>
       <tr>
        <td>Ana Benito</td>
        <td>Riccardo Paladin</td>
      </tr>
      </tr>
       <tr>
        <td>Ignacio perez Meroño </td>
        <td>Giovanni Loiudice</td>
      </tr>
      </tr>
      <tr>
        <td>Maria Chara Moccia </td>
        <td>Maria Paolicelli</td>
      </tr>
      <tr>
        <td>Isaac Abril< </td>
        <td></td>
      </tr>
    </tbody>
  </table>

  <table class="alt" width=50%>
    <thead>
      <tr>
        <th>Double Bass</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Antonio Sciancalepore (principal)</td>
      </tr>
      <tr>
        <td>Marta Franco Latorre (principal)</td>
      </tr>
      <tr>
        <td>Abel Ivars Morales (principal) </td>
      </tr>
      <tr>
        <td>Ana Gonzalez Fernandez </td>
      </tr>
      <tr>
        <td>Irene Rodriguez Valles </td>
      </tr>
      <tr>
        <td>Jesus Manzanares Romero  </td>
      </tr>
      <tr>
        <td>Giancarlo Rizzo   </td>
      </tr>
      <tr>
        <td>Davide Sergi  </td>
      </tr>
      <tr>
        <td>Samuel Robles Gaitero</td>
      </tr>
      <tr>
        <td>Mattia Rossi</td>
      </tr>
      <tr>
        <td>Pablo Martinez Adan</td>
      </tr>
    </tbody>
  </table>
</div>

<a href="#TOP" class="button small">TOP</a>

## Guitars, Harp, Piano

<div class="table-wrapper" id="guitharp">
  <table class="alt">
    <thead>
      <tr>
        <th>Harp and Piano</th>
        <th>Guitars</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Carmen Escobar (Harp, principal)</td>
        <td>Daniel Garcia Garcia (principal)  </td>
      </tr>
      <tr>
        <td>Franca Parenti (Harp)</td>
        <td>Luis M. Fernandez (principal)   </td>
      </tr>
      <tr>
        <td>Claudio Carbo Montaner (Piano, principal)</td>
        <td>Maria Victoria Gorostiza </td>
      </tr>
      <tr>
        <td>Jesus Gomez Madrigal (Piano)</td>
        <td>Alfonso V. Carrascosa </td>
      </tr>
      <tr>
        <td> </td>
        <td>Juan Pablo Garcia Serrano  </td>
      </tr>
      <tr>
        <td> </td>
        <td>Ana Cheliz de Lucio</td>
      </tr>
      <tr>
        <td> </td>
        <td>Diego Sanchez Alcolea (baroque lute & arch-lute)</td>
      </tr>
      <tr>
        <td>  </td>
        <td>Fulvio Marchesin (bass) </td>
      </tr>
      <tr>
        <td>  </td>
        <td>Salvador Carbo Muntaner (bass)</td>
      </tr>
    </tbody>
  </table>
</div>

<a href="#TOP" class="button small">TOP</a>

## Woodwind

<div class="table-wrapper" id="woodwind">
  <table class="alt">
    <thead>
      <tr>
        <th>Flute</th>
        <th>Oboe</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Alberto Esteban Rojo (principal)  </td>
        <td>Lucia Valles Rodriguez (principal)</td>
      </tr>
      <tr>
        <td>Ilona Hofmanova (principal)   </td>
        <td>Miriam Aguilar Cornejo (principal)</td>
      </tr>
       <tr>
        <td>Jesus Cortes Pendon
        <td>Tomeu Gili Mulet</td>
      </tr>
       <tr>
        <td>Maria Urdiales del Campo  </td>
        <td>Debora Marquez Serrano</td>
      </tr>
    </tbody>
  </table>

  <table class="alt">
    <thead>
      <tr>
        <th>Clarinet</th>
        <th>Bassoon</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Juan Soto Alvaredo (principal) </td>
        <td>Manuel Aguilo Furio (principal)</td>
      </tr>
      <tr>
        <td>Hector J. Hurtado Salazar (principal)</td>
        <td>Mauro di Carlo</td>
      </tr>
      <tr>
        <td>Israel Fernandez Granados</td>
        <td>Julia Valles Rodriguez</td>
      </tr>
      <tr>
        <td>Maria Sanchez del Solar (Bass cl.)  </td>
        <td>  </td>
      </tr>
    </tbody>
  </table>
</div>

<a href="#TOP" class="button small">TOP</a>

## Brass

<div class="table-wrapper" id="brass">
  <table class="alt">
    <thead>
      <tr>
        <th>French Horn</th>
        <th>Trumpet</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Maria Jose Ruiz Cabello (principal)</td>
        <td>Sergio Rodriguez Herrero (principal)</td>
      </tr>
      <tr>
        <td>Maria Olivera Rodriguez (principal)</td>
        <td>Gabriel Rivera Salmeron (principal)</td>
      </tr>
       <tr>
        <td>Luis M. Orviz      </td>
        <td><Aldo Donelli/td>
      </tr>
       <tr>
        <td>Zoilo Ballester Gutierrez     </td>
        <td>Samuele Mammano</td>
      </tr>
      <tr>
        <td>Riccardo Lorenti  </td>
        <td> Samuel Ballester Gutierrez </td>
      </tr>
    </tbody>
  </table>

  <table class="alt">
    <thead>
      <tr>
        <th>Trombone</th>
        <th>Tuba</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Miguel Rivera Salmeron    </td>
        <td>David Olivera Rodriguez (principal)</td>
      </tr>
      <tr>
        <td>Jesus Gomez Madrigal</td>
        <td>Paolo Alfieri (Bombardino)</td>
      </tr>
      <tr>
        <td>Joan Gil</td>
        <td>  </td>
      </tr>
      <tr>
        <td>Riccardo Lorenti  </td>
        <td>  </td>
      </tr>
    </tbody>
  </table>
</div>

<a href="#TOP" class="button small">TOP</a>

## Percussion

<div class="table-wrapper" id="percussion">
  <table class="alt" width=50%>
    <thead>
      <tr>
        <th>Percussion</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Matteo Pietropaolo (timpani) </td>
      </tr>
      <tr>
        <td>Vizenzo Leuzzi</td>
      </tr>
       <tr>
        <td>Joaquin Molla</td>
      </tr>
       <tr>
        <td>Paula Serrano Arviza</td>
      </tr>
       <tr>
        <td>Maria Jesus Nuez</td>
      </tr>
    </tbody>
  </table>
</div>

<a href="#TOP" class="button small">TOP</a>

{{< socialLinks >}}
