+++
title = "Conciertos realizados"
weight = 20
draft = false
menuname = "Conciertos"
+++

{{< figure class="image main" src="/images/mano-Pau-BW-001-small50pct.jpg" alt="Mano del director" style="height: 200px;">}}
<div id="top">

La  Orquesta  y  Coro  del  Camino  Neocatecumenal  (OSCNC)  se  formó  en  Madrid,  el  8  de  Diciembre de 2010. Desde su creación, ha realizado conciertos en los siguientes lugares:

</div>

## 2011

* ROME: **Paul VI Hall (Nervi Hall), Vatican City**, before His Holiness Pope Benedict XVI, in an Audience with the Neocatechumenal Communities, *January 17th, 2011*
* ISRAEL: **Aula Magna,Domus Galilaeae (Korazim, Israel)**, concert for 250 Bishops from all the world, and dignitaries of the State of Israel, *January 31st, 2011*
* ISRAEL: **Aula Magna,Domus Galilaeae (Korazim, Israel)**, concert for the Jewish Community of Northern Galilee and dignitaries of the State of Israel, *April 21st, 2011*
* PARIS: **Church of Notre-Dame de Bonne Nouvelle (Paris, France)**, *March 27th, 2011*
* DUSSELDORF: **Arena Esprit (Dusseldorf, Germany)**, concert celebrated in preparation for World Youth Day 2011, before bishops and dignitaries from Germany, and 50,000 attendees, *May 29th, 2011*
* MADRID: **La Almudena Cathedral (Madrid, Spain)**, memorial concert/celebration honnoring the Holy Trinity, presided by the Cardinal of Madrid, for  dignitaries of the Diocese of Madrid and general public, *March 19th, 2011*
* MADRID: **Plaza de Cibeles (Madrid, Spain)**, concert performed in the Vocational Meeting of the Neocatechumenal Way for World Youth Day 2011, before bishops and dignitaries of the whole world, and 250,000 attendees, *August 22nd, 2011*
* ISRAEL: **Bethlehem Convention Palace and Auditorium, city of Bethlehem**, Christmas concert/celebration presided by the Jerusalem Latin Patriarch, for the christian community of Palestine, *December 27th, 2011*
* ISRAEL: **Gerard Behar Auditorium of the City of Jerusalem,** with the participation of more than 700 Jewish people and presided by Rabbi David Rosen, *December 29th, 2011*

<a href="#top" class="button small">TOP</a>
## 2012

* ROME: **Paul VI Hall (Nervi Hall), Vatican City**, before His Holiness Pope Benedict XVI, in an Audience with the Neocatechumenal Communities. *January 21st, 2012*

* **USA Tour: “A Symphonic Homage and Prayer”** (http://www.sufferingoftheinnocents.com/)  series of concerts for the Jewish and Catholic communities,  in selected USA cities:
  * **Boston Symphony Hall**, Boston MA, *May 6th 2012*
  * **Avery Fisher Hall (Lincoln Center)**, New York City NY, *May 8th 2012*
  * **Jewish Center of Teaneck**, Teaneck NJ, concert for the NJ Jewish communit,  *May 9th, 2012*
  * **Brooklyn Cathedral**, Brooklyn NY, concert presided by Brooklyn archbishop, *May 10th, 2012*  
  * **Chicago Orchestra Hall**, Chicago IL, *May 14th 2012*

<a href="#top" class="button small">TOP</a>
## 2013

* POLAND: **Auschwitz Memorial Concert**, in front of the Gates of Death of Auschwitz-Birkenau (Krakow, Poland) concentration camp, presided by Card. Stanisław Dziwisz and attended by 10,000 people, including members of the Polish Episcopal Conference, around 200 Rabbis and jewish personalities, Krakow political authorities and general public. *June 23rd 2013*
* POLAND: **Lublin Oval Square (plac Zamkowy), Lublin. Poland**. Concert to celebrate Laurea Honoris Causa in Sacred Theology to Kiko Arguello, from the prestigious John Paul II Catholic University of Lublin, and performed for  members of the JPII Catholic University, Polish Episcopal Conference, around 200 Rabbis and other jewish personalities, Lublin political authorities and general public. *June 25th, 2013*
* HUNGARY: **Hungarian State Opera House, Budapest**. Concert for the Budapest Jewish and Catholic communities, and general public. Pesided by Card. Dr. Péter Erdő. *June 27th, 2013*

<a href="#top" class="button small">TOP</a>
## 2015

* MADRID: **Caja Magica Hall, Madrid**. Concert for Madrid Neocatechumenal Communities, presided by Madrid Archbishop, D. Carlos Osoro. *March 22nd, 2015*
* ISRAEL: **Aula Magna, Domus Galilaeae (Korazim, Israel)**, concert for 200 Jewish Rabbis of the whole world, Bishops and Cardinals of the Catholic Church and dignitaries of the State of Israel. *May 5th, 2015*

<a href="#top" class="button small">TOP</a>
## 2016

* JAPAN: **Concerts “in memoriam” for the 2011 Tsunami victims**, in selected Japan cities:
  * **Main Auditorium, Fukushima city**. Presided by Mons. Hirayama and Local authorities. *May 5th, 2016*
  * **City Auditorium of Koriyama**. *May 6th, 2016*
  * **Suntory Hall, Tokyo**. *May 7th, 2016*

* ROME: **Paul VI Hall (Nervi Hall), Vatican City**, concert/celebration belonging to the acts of the Jubilee of Mercy 2016, presided by Cardinal Paul J. Cordes, for Holy See dignataries, foreign ambassadors and general public. *October 7th, 2016.*

<a href="#top" class="button small">TOP</a>
## 2017
* ISRAEL: **Aula Magna, Domus Galilaeae (Korazim, Israel)**, concert for 400 Jewish Rabbis of the whole world, Bishops and Cardinals of the Catholic Church, and dignitaries of the State of Israel. *May 2nd, 2017*

* TRIESTE: **Piazza Unità d'Italia, Trieste**, concierto homenaje a las victimas de la _Soah_ organizado por la Diócesis, Aytuntamiento y Comunidad Judía de Trieste. *25 de Junio, 2017*

<a href="#top" class="button small">TOP</a>
## 2018
* SORIA: **Concatedral de San Pedro, Soria**, concierto en honor de Carmen Hernandez (http://soriaconcierto.es/), en el Año Jubilar concedido por el Papa Francisco a las las Hermanas Clarisas de Soria. Celebrado ante personalidades civiles y eclesiasticas, presidido por el obispo de Soria y al que asistieron mas de 4000 personas. *27 de Mayo, 2018*

* BERLIN: **Berliner Philharmoniker Hall, Berlin**, concierto en honor de las victimas del Holocausto (http://dasleidenderunschuldigen.de/), presidido por el Obispo de Berlin, ante personalidades diplomaticas, eclesiasticas y del mundo judio. *10 de Junio, 2018*

<a href="#top" class="button small">TOP</a>

## 2019
* BARI: **Palaflorio Bari, Italy**, concierto en honor a los MArtires de Albania. Presidido por el obispo emerito de Palermo, Mons. Paolo Romeo. *2 de Junio de 2019*
http://www.srmalbania.org/sinfoniamatera2019

<a href="#top" class="button small">TOP</a>
## 2021
* Madrid: **Universidad Francisco de Vitoria, Madrid**, *Honoris Causa* doctorate ceremony for David Rosen and Kiko Arguello, a reduced version of the Symphony was performed by string quartet, piano and choir.  *October 25th, 2021*
https://vimeo.com/618762696#t=5585s

* Roma: **Basilica San Paolo Fuori la mura, Roma**, Presentation of the book "Carmen Hernandez, Biographic Notes". *November 29th, 2021*
https://youtu.be/vtPLc0Zn2Go?t=8380

<a href="#top" class="button small">TOP</a>
## 2022
* Madrid: **Universidad Francisco de Vitoria, Madrid**, *HOpening of the Beatification Cause for Carmen hernandez.  *December 4th, 2022*
https://vimeo.com/manage/videos/787377888/8508af7175

<a href="#top" class="button small">TOP</a>
## 2023
* Trieste: **Teatro Giuseppe Verdi, Trieste**, World premiere of the symphonic poem "El Mesias". *November 19th, 2023*
https://youtu.be/vtPLc0Zn2Go?t=8380https://www.youtube.com/watch?v=ivD7AAEIIR0&t=2430s

<a href="#top" class="button small">TOP</a>

